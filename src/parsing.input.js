/**
 * To calculate minimum number of coins needed to make given amount.
 * BBC Web developer test
 * July 2017
 */
 var Parsing = {

   new: function(str) {
    var convert = false;
    if (this.isPound(str)) {
      convert = true;
    }

    str = this.removeNonNumeric(str);

    var num = parseFloat(str);

    if (this.isDecimal(num) || convert) {
      num = this.convertToPennies(num);
    } 

    return num;
  },

  isPound: function(str) {
    return (str.indexOf('£') !== -1);
  },

  removeNonNumeric: function(str) {
    return str.replace(/[£p]+/g, '');
  },

  isDecimal: function(num) {
    return (num % 1 !== 0);
  },

  convertToPennies: function(num) {
    return (num.toFixed(2) * 100);
  }
};