/**
 * To calculate minimum number of coins needed to make given amount.
 * BBC Web developer test
 * July 2017
 */
 $(function() {

  $('#pennies-input').bind('keypress', function(e) {
    if (e.which === 13) {

      e.preventDefault();

      Helper.hideElements();
      
      var response = Validation.checkInput(this.value)
      if (response.status === true) {

        var amount = Parsing.new(this.value);

        var results = Calculation.minimumCoins(amount);
        Helper.displayResult(results);
      } else {
        Helper.displayError(response.message);
      }
    }
  });
});

 var Helper = {

  hideElements: function() {
    $('#error-alert').hide();
    $('#results-panel').hide();
  },

  displayError: function(msg) {
    $('#error-alert').html('<p><strong>Ooops!</strong>: ' + msg + '</p>')
    $('#error-alert').show();
  },

  displayResult: function(hash) {
    var html = this.formHTML(hash);
    $('#results').html(html);
    $('#results-panel').show();
  },


/*
* Result
*/
formHTML: function(hash) {
  var html = '<ul>'
  for (var key in hash) {
    html += '<li>' + hash[key] + ' x ' + this.transformKey(key) + '</li>';
  }
  html += '</ul>'
  return html;
},

transformKey: function(key) {
  if (key === '100' || key === '200') {
    key = '£' + key[0];
  } else {
    key += 'p';
  }
  return key;
}
};
