/**
 * To calculate minimum number of coins needed to make given amount.
 * BBC Web developer test
 * July 2017
 */
 var Validation = {

   checkInput: function(str) {
    str = this.removeWhitespace(str);

    var response = {
      'status': false,
      'message': ''
    };

    if (!str) {
      response.message = 'You have not entered any number';
    } else if (str === '0'){
      response.message = 'Unable to compute. Value must be greater than zero';
    } else if (this.containsAlpha(str)) {
      response.message = 'Character entered does not match a valid number of sterling coin';
    } else if (this.containsNoNumeric(str)) {
      response.message = 'You have not entered any number';
    } else {
      response.status = true;
    }

    return response;
  },
  
  removeWhitespace: function(str) {
    return str.replace(/\s+/g, '');
  },

  containsAlpha: function(str) {
    var regex = /[^£.p\d]/g;
    return regex.test(str);
  },

  containsNoNumeric: function(str) {
    var regex = /\d+/g;
    return !regex.test(str);
  }
};