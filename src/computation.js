/**
 * To calculate minimum number of coins needed to make given amount.
 * BBC Web developer test
 * July 2017
 */
 var Calculation = {

  currency: [200, 100, 50, 20, 10, 5, 2, 1],
  minimumCoins: function(pennies) {
    var results = {},
    currentCoin;

    var x = 0;

    while (pennies) {
      
      currentCoin = this.currency[x++];

      if (pennies >= currentCoin) {
        results[currentCoin] = this.numberOfCoins(pennies, currentCoin);
        pennies = this.remainingPennies(pennies, currentCoin);
      }
    }

    return results;
  },


  numberOfCoins: function(pennies, coin) {
    return Math.floor(pennies / coin);
  },

  remainingPennies: function(pennies, coin) {
    return pennies % coin;
  }
};