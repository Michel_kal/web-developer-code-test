# BBC Web Developer Code Test

# =========== Problem statement ===============

Given a number of pennies - calculate the minimum number of Sterling coins needed to
make that amount
--------------------------------		

# How to use it

Clone the repository, navigate to `web-developer-code-test/views/` Open `index.html` in a browser. Enter a Sterling amount (e.g. £1.23) and press Enter on your keyboard.

# Files
All the javascripts used are included in 

### `src/`

Html file required for the UI, including the external libraries used are included in 
([Bootstrap](http://getbootstrap.com/), [jQuery](http://jquery.com/)).

### `views/`


## Accessibility

The Bootstrap CSS framework was utilise for easy accessibility and responsiveness on mobile, desktop and tablet.





